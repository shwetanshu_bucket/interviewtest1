﻿namespace Domain.Request
{
    /// <summary>
    /// Number converter request
    /// </summary>
    public class Request
    {
        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Input number
        /// </summary>
        public decimal Number { get; set; }
    }
}
