﻿namespace Domain.Response
{
    /// <summary>
    /// Response for number converter
    /// </summary>
    public class Response
    {
        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Converted words from number
        /// </summary>
        public string ConvertedText { get; set; }
    }
}
