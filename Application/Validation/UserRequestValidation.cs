﻿using Domain.Request;
using FluentValidation;

namespace Application.Validation
{
    public class UserRequestValidation : AbstractValidator<Request>
    {
        public UserRequestValidation()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("User name can not be blank.");
            RuleFor(x => x.Number).GreaterThan(0).WithMessage("Enter number more than zero.");
        }
    }
}
