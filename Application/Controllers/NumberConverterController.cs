﻿using Domain.Request;
using Domain.Response;
using Infrastructure.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace InterviewTest.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class NumberConverterController : ControllerBase
    {
        private readonly ILogger<NumberConverterController> _logger;
        private readonly INumberConverter _numberConverter;

        public NumberConverterController(ILogger<NumberConverterController> logger, INumberConverter numberConverter)
        {
            _logger = logger;
            _numberConverter = numberConverter;
        }

        [HttpGet]
        public string HealthCheck()
        {
            return "Health check ok";
        }

        [HttpPost]
        public IActionResult Convert(Request request)
        {
            try
            {
                var response = new Response()
                {
                    UserName = request.UserName,
                    ConvertedText = _numberConverter.ConvertNumberIntoWords(request.Number).ToUpper()
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Ok("Failed to convert");
            }
        }
    }
}
