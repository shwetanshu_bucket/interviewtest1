﻿namespace Infrastructure.Services.Contracts
{
    /// <summary>
    /// Number converter into words
    /// </summary>
    public interface INumberConverter
    {
        /// <summary>
        /// Convert number into words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        string ConvertNumberIntoWords(decimal number);
    }
}
